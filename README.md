# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Konovalov Petr

E-MAIL: pkonjob@yandex.ru

E-MAIL: pkonovalov@tsconsulting.ru

PHONE-NUMBER: 89060362058

# SOFTWARE

* JDK 1.8.0 

* Windows

# BUILD PROGRAM

```
mvn clean install
```
# RUN PROGRAM

```
java -jar ./task-manager.jar
```
